(function () {
    'use strict';

    angular.module('app').config(RouteConfig);

    function RouteConfig($routeProvider) {
        $routeProvider.when('/search', {
            templateUrl : 'html/search.html',
            controller : 'SearchCtrl',
            controllerAs : 'vm'
        }).when('/new/:id', {
            templateUrl : 'html/new.html',
            controller : 'NewCtrl',
            controllerAs : 'vm'
        }).when('/new', {
            templateUrl : 'html/new.html',
            controller : 'NewCtrl',
            controllerAs : 'vm'
        }).otherwise('/search');
    }
}) ();