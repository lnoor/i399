(function () {
    'use strict';

    angular.module('app').controller('NewCtrl', NCtrl);

    function NCtrl($http, $routeParams, $location) {
        var vm = this;

        vm.newContact = new contact('', '');
        vm.addNew = addNew;
        vm.editing = false;
        editRow();
        init();

        function addNew() {
            if($routeParams.id !== undefined) {
                $http.put('api/contacts/' + $routeParams.id, vm.newContact).then(function () {
                    $location.path('/search');
                });
            } else {
                $http.post('api/contacts', vm.newContact).then(function () {
                    $location.path('/search');
                });
            }
        }

        function contact(name, phone) {
            this.name = name;
            this.phone = phone;
        }

        function init() {
            $http.get('api/contacts').then(function (result) {
                vm.contacts = result.data;
            });
        }

        function editRow() {
            if($routeParams.id !== undefined) {
                $http.get('api/contacts/' + $routeParams.id).then(function (result) {
                    vm.editing = true;
                    vm.newContact = result.data;
                });
            }
        }
    }
}) ();
