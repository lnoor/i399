(function () {
    'use strict';

    angular.module('app').controller('SearchCtrl', SCtrl);

    function SCtrl($http, modalService) {
        var vm = this;
        vm.contacts = [];
        vm.removeItem = removeItem;

        init();

        function init() {
            $http.get('api/contacts').then(function (result) {
                vm.contacts = result.data;
            });
        };

        function removeItem(id) {
            modalService.confirm()
                .then(function () {
                return $http.delete('api/contacts/' + id);
            }).then(init);
        };
    }
}) ();